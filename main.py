#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 21:48:53 2019

@author: bakar
"""
from test import Graph
from read_graphs import TwitterGraph,RoadGraph
from matplotlib import pylab
import networkx as nx
import queue
import matplotlib.pyplot as plt

'''
### testing
G = Graph(False,100)
G.add_edge(1,2)
G.add_edge(1,3)
G.add_edge(1,4)
G.add_edge(1,5)
G.add_edge(3,4)
G.add_edge(2,6)
G.add_edge(3,7)
G.add_edge(4,8)
G.add_edge(8,9)
G.add_edge(8,10)


G.k_neighborhood(1,2)
G.show_graph()

G.k_path(1,3)
G.show_graph()

G.independent_set(1,3)
G.show_graph()
'''

### ROAD GRAPH####

G2 = RoadGraph()

G2.k_neighborhood(1,5)
G2.show_graph()


G2.k_path(1056910,20)
G2.show_graph()

G2.independent_set(1,10)
G2.show_graph()


"""
### TWITTER GRAPH ###

G3 = TwitterGraph()


G3.k_neighborhood(1,5)
G3.show_graph()


G3.k_path(1, 3)
G3.show_graph()

G3.independent_set(26,2)
G3.show_graph()
"""