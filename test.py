#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 12:12:13 2019

@author: bakar
"""

"""
Task:
    
For this project, you will have to implement three operations over large graphs.

    (a) Display the k-hop neighborhood of a given node u.
    (b) Find and display a path of length k, that has node u as one of the endpoints
    (c) Display a subgraph containing an independent set of size k that includes node u. Display the nodes in the independent set with a different color.

For your experiments, you will use the TWITTER-REAL-GRAPH-PARTIAL graph which has 580.8k nodes and the ROADNET-PA graph which has 1.1M nodes.

"""

import networkx as nx
import queue
import matplotlib.pyplot as plt

class Graph:
    def __init__(self,directed,number_of_nodes):

        self.adj = []
        self.number_of_nodes = number_of_nodes

        for i in range(0,self.number_of_nodes):
            self.adj.append([])
        self.directed = directed



    def add_edge(self,a,b):
        self.adj[a].append(b)
        if self.directed == False:
            self.adj[b].append(a)



    def show_graph(self):
        pos = nx.spring_layout(self.G)
        nx.draw(self.G,pos = pos,node_color = self.color_map,with_labels=True, font_weight='bold',node_size = 200)
        plt.show()


####### Task (a): Display the k-hop neighborhood of a given node u.
    def k_neighborhood(self,u,k):
        self.G = nx.Graph() ## create the graph
        self.color_map = [] ## what colour will be for the node
       

        dist = []
        for i in range(0,self.number_of_nodes):
            dist.append(-1)

        dist[u] = 0
        self.G.add_node(u)

        self.color_map.append('red')
        Q = queue.Queue(maxsize=self.number_of_nodes) #initialize queue
        Q.put(u) #insert element

        # flag
        out = False
        while Q.empty()==False:
            if out == True:
                break
            node_u = Q.get() #get and remove the element
            for v in self.adj[node_u]:
                if dist[v] == -1:
                    dist[v] = dist[node_u] + 1
                    Q.put(v)
                    if dist[v] <= k: ##  check if node v is in the k-neighborhood
                        self.G.add_edge(node_u,v)
                        self.color_map.append('blue')
                    else:
                        out = True
                        break




############ Task (b): Find and display a path of length k, that has node u as one of the endpoints

    def paint_path(self,v,parent):
        if parent[v] == -1:
            return

        self.paint_path(parent[v],parent)
        self.G.add_node(parent[v])
        self.color_map.append('blue')
        self.G.add_edge(parent[v],v)


    def k_path(self,u,k):
        k -= 1  
        self.G = nx.Graph()
        self.color_map = []
       

        dist = []
        parent = []
        for i in range(0,self.number_of_nodes):
            dist.append(-1)
            parent.append(-1)

        dist[u] = 0;
        Q = queue.Queue(maxsize=self.number_of_nodes)
        Q.put(u)
        possible = -1  #initially declared, if no path exist
        out = False
        ## apply BFS
        while Q.empty() == False:
            if out == True:
                break
            node_u = Q.get()
            for v in self.adj[node_u]:
                if dist[v] == -1:
                    parent[v] = node_u
                    dist[v] = dist[node_u] + 1
                    Q.put(v)
                    if dist[v] == k:
                        possible = v
                        out = True
                        break
        # end of BFS
        if possible == -1:
            print("There is not path of length " + str(k+1))
            self.G = nx.Graph()

            return False


        self.G.add_node(possible)
        self.color_map.append('blue')
        self.paint_path(possible,parent)
        return True





#####Task (c): Display a subgraph containing an independent set of size k that includes node u. Display the nodes in the independent set with a different color.

    def independent_set(self,u,k):
        self.G = nx.Graph()
        self.color_map = []
        # if self.directed == True:
        #     self.G = nx.DiGraph()

        dist = []
        parent = []
        for i in range(0,self.number_of_nodes):
            dist.append(-1)
            parent.append(-1)
        out  = False
        if k == 1:
            out = True # base case: if there is only one root in system
            
        #base case    
        root = u
        num_k = 1 #number of nodes in Independent set
        
        while num_k < k:
            
            if root != u: 
            # find one node that its not visited yet
                for i in range(1,self.number_of_nodes):
                    if dist[i] == -1:
                        root = i #take all other nodes except the previous subgraph
                        break
            if root == -1: #all nodes in the graph has already visited
                break
            
            dist[root] = 0;
            Q = queue.Queue(maxsize=self.number_of_nodes)
            Q.put(root) # this is a root node
            self.G.add_node(root)
            self.color_map.append('blue')
            
            if root!=u:
                num_k = num_k + 1
            if num_k == k:
                out =True 
    
            while Q.empty() == False:
                if out == True:
                    break
                node_u = Q.get() # consider each layer as node_u one by one
                for v in self.adj[node_u]: # take the adjacent of node_u
                    if dist[v] == -1: #initially all distance are infinity (according to book), here, I use -1. that means the node
                                      #is not visited
                        parent[v] = node_u
                        dist[v] = dist[node_u] + 1 #in each iteration, the distance increase by 1 from parent to its children
                        self.G.add_node(v)         #add node in the graph, it means v nodes, suppose for the first  iteration, 
                                                   #v will be first layer, second iteration, v will be second layer. each item it's add 
                                                   #the nodes of layer, and the graph will expand by adding layer by layer 
                        self.G.add_edge(node_u,v)
                        if dist[v] % 2 == 0:       #the distance betwenen root and first layer is one, (1%2) not equal to zero. 
                                                   #so, root, and first layer are not independent set(IS)
                                                   #but distance(root, second layer) = 2, and (2%2)=> divisible by 2, so they are IS
                            num_k =  num_k + 1     
                            self.color_map.append('blue')
                        else:
                            self.color_map.append('green')
                        Q.put(v)
        
                        if num_k == k :
                            out = True
                            break    
                        
            root = -1 #I want to check other components that do not contain u
                
        if out == False:
            print("There is not independent set of length " + str(k))
            self.G = nx.Graph()
        return out

test.py
Displaying test.py.