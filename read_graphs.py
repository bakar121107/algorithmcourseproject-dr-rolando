

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 17:33:26 2019

@author: bakar
"""

from test import Graph

def TwitterGraph():
    f = open("/home/bakar/Music/Dia del Muertos/alg/TWITTER-Real-Graph-Partial.edges","r")
    lines = f.readlines()
    #print(lines[5]) # it'll print the partial edges from the graph, here 2,3
    maxi = 0
    for i in lines:
        if len(i)!=0 : # for example if i=5, the lengthy will be 2
            numbers = i.split(",") 
            x = int(numbers[0])
            y = int(numbers[1])
            maxi = max(maxi,x)
            maxi = max(maxi,y)
            # after this for loop, it'll find the a node that is maximum number in Twitter graph

    G3 = Graph(True,maxi+1) # the function is called from test.py python code 
                            # and here in parenthesis, True means it's directed graph



    ### create graph
    for i in lines:
        if len(i)!=0 :
            numbers = i.split(",")
            x = int(numbers[0])
            y = int(numbers[1])
            G3.add_edge(x,y)
    return G3



def max(a,b):
    if a>b:
        return a
    return b

def RoadGraph():
    f = open("/home/bakar/Music/Dia del Muertos/alg/road-roadNet-PA.mtx","r")
    lines = f.readlines()
    maxi = 0
    for i in lines:
        if len(i)!=0 :
            numbers = i.split()
            x = int(numbers[0])
            y = int(numbers[1])
            maxi = max(maxi,x)
            maxi = max(maxi,y)
 
    G2 = Graph(False,maxi+1) # the function is called from test.py python code 
                            # and here in parenthesis, False means it's undirected graph


    ### create graph

    for i in lines:
        if len(i)!=0 :
            numbers = i.split()
            x = int(numbers[0])
            y = int(numbers[1])
            G2.add_edge(x,y)

    return G2
